<?php
$lang['welcome_message'] = 'أهلاً بك في موقعنا!';
$lang['home'] = 'الرئيسية';
$lang['IPTV'] = ' باقات IPTv';
$lang['pricing_pilling'] = 'طرق الدفع والأسعار ';
$lang['CCcam'] = 'أشتراك CCcam ';
$lang['payment'] = 'طــرق الدفــع';
$lang['pricing'] = 'خطط الأسعـــار';
$lang['support'] = 'الدعم الفنى';
$lang['faq'] = 'أسئلة شائعة';
$lang['why_choose_us'] = 'لما نحن الاختيار الأمثل';
$lang['why_choose_us_text'] = 'الأكثر عددا للقنوات ... الأثبت فى المشاهدة ... الأسرع فى الدعم
<br>والتجربة خير دليل على ذلك ..';
$lang['strategy'] = 'أستراتجيتنا';
$lang['strategy_text'] = 'لدينا خطة عمل واضحة وضعناها مسبقا لتشمل أدق التفاصيل التى يهتم بها اغلب العملاء والمشاهدين حول العالم';
$lang['explore'] = 'اكتشافتنا';
$lang['explore_text'] = 'سوف تنبهر دائما معنا بكل معانى الكلمة فنحن نقدم كل  ما هو جديد دائما فى هذا المجال قبل ان يصل إليه غيرنا ';
$lang['expertise'] = 'خبرتنا';
$lang['expertise_text'] = 'قضائنا العديد من السنوات فى هذا المجال جعل منا نضع يدينا على اكثر الثغرات التى نتجنبها دائما قبل ان تزعج مشاهدينا';
$lang['see_more'] = 'شاهد المزيد';
$lang['follow'] = 'تابعنا';
$lang['rights'] = ' ماكشيرينج جميع الحقوق محفوظة';
$lang['payment_page'] = ' طــرق الدفـع';
$lang['cccam_page'] = 'إشتراك CCcam';
$lang['cccam_pricing'] = 'خطط أسعار CCcam';
$lang['iptv_pricing'] = 'خطط أسعار IPTv';
$lang['order'] = 'أطلب الأن';
$lang['maksharing_title'] = 'ماكشيرينج | PRO IPTV | Premium IPTV Subscription | Buy IPTV';
$lang['meta_description'] = ' اشتراك سيرفر iptv ماك شيرينج لأفضل وأرخص IPTV و CCcam القنوات العربية  - للجالية المصرية و العربية المغتربين فى امريكا و اوروبا و اشقاؤنا العرب فى الدول العربية و جميع ';
$lang['meta_keywords'] = 'أشتري iptv, ماكشيرينج, مصر, مصري iptv, أفضل iptv, مصري cccam, أفضل cccam, أفضل السيرفرات,iptv مقدم,iptv d,iptv server,iptv box, free iptv, smart iptv, iptv اشتراك, مدفوع iptv, internet tv, live stream, mag iptv, iptv خدمة, iptv قنوات';
$lang['videos'] = 'شروحات مصورة ';
$lang['welcome_page1'] = 'نأخذ الترفيه الخاص بك إلى أعلى مستوى';
$lang['welcome_page2'] = 'عيش الفارق';
$lang['welcome_page3'] = 'ترفيهك يهمنا';
$lang['welcome_page4'] = 'هل انت مستعد؟';
$lang['happy_clients_title'] = 'عملاء سعداء';
$lang['happy_clients'] = 'ما نسعى للوصول إليه من خلال خدمتنا
أن نحظى بسعادتكم و رضائكم أينما كنتم
وأهتماماتكم التى تبحثون عن متابعتها
سوف نحاول تقديمها لكم فى أفضل صورة دائما
';
$lang['saving'] = 'عـروض التـوفير';
$lang['why_us'] = 'لماذا نحن؟';