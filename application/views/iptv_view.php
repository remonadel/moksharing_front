<?php $this->load->view("header"); ?>
<aside id="fh5co-hero" clsas="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">

			<li style="background-image: url(<?= UPLOADS ?>slide_3.jpg);">
				<div class="overlay-gradient"></div>
				<div class="container">
					<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
						<div class="slider-text-inner">
							<h2><?= $this->lang->line('IPTV'); ?></h2>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>

<div id="fh5co-grid-products" class="animate-box">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-offset-2 fh5co-heading">
					<div class="panel-group" id="accordion">

						<?php if(isset($categories)): ?>
							<?php foreach($categories as $category): ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$category['id'] ?>">
												<img style="width:30px" src="<?= UPLOADS.'categories_icons/'.$category['icon'] ?>">  <?=$category['name_'.$this->session->userdata['lang']]?></a>
										</h4>
									</div>
									<div id="collapse<?=$category['id'] ?>" class="panel-collapse collapse">
										<div class="panel-body">
											<table class="table table-bordered">

												<tbody>

												<?php $rows_count = ceil(count($category['channels'])/3) ?>
												<?php for ($i=0 ; $i < $rows_count ; $i++): ?>
												<tr>
													<?php for ($j=0 ; $j <= 2 ; $j++): ?>

														<td><?= $category['channels'][$j+$i]['name'] ?></td>

													<?php endfor; ?>
												</tr>
												<?php endfor; ?>


												</tbody>
											</table>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>

					</div>


			</div>
		</div>
	</div>



</div>

</div>


<?php $this->load->view("footer"); ?>
