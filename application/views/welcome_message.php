<?php $this->load->view("header"); ?>
<aside id="fh5co-hero" class="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">
			<?php foreach($main_slider_banners as $main_slider_banner): ?>
				<?php if($main_slider_banner['image'] != ''): ?>
					<li style="background-image: url(<?= BANNERS ?>/<?=$main_slider_banner['image'] ?>);">
						<div class="overlay-gradient"></div>
						<div class="container">
							<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
								<div class="slider-text-inner">
									<h2><?= $main_slider_banner['description_'.$this->session->userdata('lang')] ?></h2>
									<p><a href="<?= $main_slider_banner['btn_link'] ?>" class="btn btn-primary btn-lg"><?= $main_slider_banner['btn_text_'.$this->session->userdata('lang')] ?></a></p>
								</div>
							</div>
						</div>
					</li>
				<?php endif; ?>
			<?php endforeach; ?>
		</ul>
	</div>
</aside>

<div id="fh5co-why-us" class="animate-box">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center fh5co-heading" style="width:52% !important;">
				<h2><?= $this->lang->line('why_choose_us'); ?></h2>
				<p><?= $this->lang->line('why_choose_us_text'); ?> </p>
				<p><a href="<?= site_url() ?>/welcome_page/index/3" class="btn btn-primary btn-lg"><?= $this->lang->line('see_more'); ?></a></p>
			</div>
			<div class="col-md-4 text-center item-block">
				<span class="icon"><img src="<?= ASSETS ?>images/30.svg" alt="Free HTML5 Templates" class="img-responsive"></span>
				<h3><?= $this->lang->line('strategy'); ?></h3>
				<p><?= $this->lang->line('strategy_text'); ?></p>
				</div>
			<div class="col-md-4 text-center item-block">
				<span class="icon"><img src="<?= ASSETS ?>images/18.svg" alt="Free HTML5 Templates" class="img-responsive"></span>
				<h3><?= $this->lang->line('explore'); ?></h3>
				<p><?= $this->lang->line('explore_text'); ?></p>
				</div>
			<div class="col-md-4 text-center item-block">
				<span class="icon"><img src="<?= ASSETS ?>images/expertise.png" alt="Free HTML5 Templates" class="img-responsive"></span>
				<h3><?= $this->lang->line('expertise'); ?></h3>
				<p><?= $this->lang->line('expertise_text'); ?></p>
			</div>
		</div>
	</div>
</div>

<div class="col-md-1">
	</div>
<div class="col-md-9">
	<aside id="fh5co-hero">
		<div class="flexslider" style="margin-bottom: -250px;margin-top: -100px;">
			<ul class="slides" id="slides2">
				<?php foreach($second_slider_banners as $second_slider_banner): ?>
					<?php if($second_slider_banner['image'] != ''): ?>
						<li style="background-image: url(<?= BANNERS ?>/<?=$second_slider_banner['image'] ?>);">
							<div class="overlay-gradient"></div>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	</aside>

</div>

<div id="fh5co-testimonial" class="animate-box">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
				<h2><?= $this->lang->line('happy_clients_title'); ?></h2>
				<p><?= $this->lang->line('happy_clients'); ?></p>
			</div>

		</div>
	</div>
</div>
<!--
<div id="fh5co-blog" class="animate-box">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
				<h2>Latest <em>from</em> Blog</h2>
				<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-md-4">
				<a class="fh5co-entry" href="#">
					<figure>
						<img src="<?= ASSETS ?>images/image_1.jpg" alt="Free Website Template, Free HTML5 Bootstrap Template" class="img-responsive">
					</figure>
					<div class="fh5co-copy">
						<h3>We Create Awesome Free Templates</h3>
						<span class="fh5co-date">June 8, 2016</span>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a class="fh5co-entry" href="#">
					<figure>
						<img src="<?= ASSETS ?>images/image_2.jpg" alt="Free Website Template, Free HTML5 Bootstrap Template" class="img-responsive">
					</figure>
					<div class="fh5co-copy">
						<h3>Handcrafted Using CSS3 &amp; HTML5</h3>
						<span class="fh5co-date">June 8, 2016</span>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					</div>
				</a>
			</div>
			<div class="col-md-4">
				<a class="fh5co-entry" href="#">
					<figure>
						<img src="<?= ASSETS ?>images/image_3.jpg" alt="Free Website Template, Free HTML5 Bootstrap Template" class="img-responsive">
					</figure>
					<div class="fh5co-copy">
						<h3>We Try To Update The Site Everyday</h3>
						<span class="fh5co-date">June 8, 2016</span>
						<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
					</div>
				</a>
			</div>
			<div class="col-md-12 text-center">
				<p><a href="#" class="btn btn-primary btn-outline with-arrow">View More Posts <i class="icon-arrow-right"></i></a></p>
			</div>
		</div>
	</div>
</div>
-->


<?php $this->load->view("footer"); ?>
