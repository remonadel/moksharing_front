<?php $this->load->view("header"); ?>
<aside id="fh5co-hero" clsas="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">

			<li style="background-image: url(<?= UPLOADS ?>slide_3.jpg);">
				<div class="overlay-gradient"></div>
				<div class="container">
					<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
						<div class="slider-text-inner">
							<h2><?= $this->lang->line('faq'); ?></h2>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>

<div id="fh5co-grid-products" class="animate-box">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-offset-2 fh5co-heading">
					<div class="panel-group" id="accordion">

						<?php if(isset($questions) AND (count($questions) > 0)): ?>
							<?php foreach($questions as $question): ?>
								<div class="panel panel-default">
									<div class="panel-heading">
										<h4 class="panel-title">
											<a class="accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapse<?=$question['id'] ?>"><?=$question['question_'.$this->session->userdata['lang']]?></a>
										</h4>
									</div>
									<div id="collapse<?=$question['id'] ?>" class="panel-collapse collapse">
										<div class="panel-body">
											 <?= str_replace(PHP_EOL, '<br>',$question['answer_'.$this->session->userdata['lang']]) ?>
										</div>
									</div>
								</div>
							<?php endforeach; ?>
						<?php endif; ?>

					</div>


			</div>
		</div>
	</div>



</div>

</div>


<?php $this->load->view("footer"); ?>
