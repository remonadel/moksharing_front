
<footer id="fh5co-footer" role="contentinfo">

    <div class="container">

        <div class="col-md-12">
            <h2><?= $this->lang->line('follow'); ?></h2>
            <ul class="fh5co-social">
                <li><a href="https://twitter.com/" target="_blank" ><i class="icon-twitter"></i></a></li>
                <li><a href="https://www.facebook.com/MakSharing-1788108024774472/" target="_blank" ><i class="icon-facebook"></i></a></li>
                <li><a href="https://plus.google.com/" target="_blank" ><i class="icon-google-plus"></i></a></li>
                <li><a href="https://www.instagram.com" target="_blank" ><i class="icon-instagram"></i></a></li>
            </ul>
        </div>


        <div class="col-md-12 fh5co-copyright text-center">
            <p>&copy; 2016 <?= $this->lang->line('rights'); ?>.<!-- <span>Developed By <a href="mailto:engremon.fci@gmail.com" >Remon Adel</a></span>--></p>
        </div>

    </div>
</footer>
</div>


<!-- jQuery -->
<script src="<?= ASSETS ?>js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="<?= ASSETS ?>js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="<?= ASSETS ?>js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="<?= ASSETS ?>js/jquery.waypoints.min.js"></script>
<!-- Flexslider -->
<script src="<?= ASSETS ?>js/jquery.flexslider-min.js"></script>

<!-- MAIN JS -->
<script src="<?= ASSETS ?>js/main.js"></script>

<script src="<?= ASSETS ?>js/jquery.bootstrap-dropdown-hover.js"></script>
<script>
    $( document ).ready(function() {
        $.fn.bootstrapDropdownHover();
    });
    </script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/582b38f2de6cd808f3089131/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>