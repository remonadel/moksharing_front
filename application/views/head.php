
<!DOCTYPE html>
    <html <?php if($this->session->userdata('site_lang') == 'arabic') echo 'class="arabic"'?>>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title><?= @$title ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?= @$this->lang->line('meta_description'); ?>" />
    <meta name="keywords" content="<?= @$this->lang->line('meta_keywords'); ?>" />
    <meta name="author" content="maksharing.com" />

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content="<?= @$title ?>"/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content="http://<?= @$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>"/>
    <meta property="og:site_name" content="maksharing.com"/>
    <meta property="og:description" content="<?= @$this->lang->line('meta_description'); ?>"/>
    <meta name="twitter:title" content="<?= @$title ?>" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="http://<?= @$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']?>" />


    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel="shortcut icon" href="favicon.ico">

    <!-- <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,400italic,700' rel='stylesheet' type='text/css'> -->
    <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet">
    <!-- Animate.css -->
    <link rel="stylesheet" href="<?= ASSETS ?>css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?= ASSETS ?>css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?= ASSETS ?>css/bootstrap.css">
    <!-- Flexslider  -->
    <link rel="stylesheet" href="<?= ASSETS ?>css/flexslider.css">
    <!-- Theme style  -->
    <link rel="stylesheet" href="<?= ASSETS ?>css/style.css">

    <!-- Modernizr JS -->
    <script src="<?= ASSETS ?>js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="<?= ASSETS ?>js/respond.min.js"></script>
    <![endif]-->

</head>