<?php $this->load->view("header"); ?>
<aside id="fh5co-hero" clsas="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">

			<li style="background-image: url(<?= UPLOADS ?>slide_3.jpg);">
				<div class="overlay-gradient"></div>
				<div class="container">
					<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
						<div class="slider-text-inner">
							<?php if($page['id'] == 1): ?>
							<h2><?= $this->lang->line('payment'); ?></h2>
							<?php elseif($page['id'] == 2): ?>
							<h2><?= $this->lang->line('CCcam'); ?></h2>
							<?php else: ?>
								<h2><?= $this->lang->line($page['name']); ?></h2>
							<?php endif; ?>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>



<div id="fh5co-grid-products" class="animate-box">
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-offset-1 fh5co-heading">
				<?php if(isset($page['content_'.$this->session->userdata['lang']]))
					echo str_replace('../../', UPLOADROOT,htmlspecialchars_decode($page['content_'.$this->session->userdata['lang']])); ?>
			</div>
		</div>
	</div>



</div>


<?php $this->load->view("footer"); ?>
