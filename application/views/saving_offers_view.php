<?php $this->load->view("header"); ?>
<aside id="fh5co-hero" clsas="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">

			<li style="background-image: url(<?= UPLOADS ?>slide_3.jpg);">
				<div class="overlay-gradient"></div>
				<div class="container">
					<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
						<div class="slider-text-inner">
							<h2><?= $this->lang->line('saving'); ?></h2>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>
<div id="fh5co-why-us" class="animate-box">
	<div class="container">
		<div class="row">

		</div>
	</div>
</div>
<div class="col-md-1">
</div>
<div class="col-md-9">
	<aside id="fh5co-hero">
		<div class="flexslider" style="margin-bottom: -250px;margin-top: -100px;">
			<ul class="slides" id="slides2">
				<?php foreach($banners as $banner): ?>
					<?php if($banner['image'] != ''): ?>
						<li style="background-image: url(<?= BANNERS ?>/<?=$banner['image'] ?>);">
							<div class="overlay-gradient"></div>
						</li>
					<?php endif; ?>
				<?php endforeach; ?>
			</ul>
		</div>
	</aside>

</div>

</div>


<?php $this->load->view("footer"); ?>
