<?php $this->load->view("head.php"); ?>
<?php
if ($this->session->userdata('site_lang') == 'arabic')
    $this->session->set_userdata('lang', 'ar');
else
    $this->session->set_userdata('lang', 'en');
?>

<body>
<div id="fh5co-page">
    <header id="fh5co-header" role="banner">
        <div class="container">
            <li class="header-inner">
                <h1 class="logo"><a href="<?= site_url()?>"><span><img style=" margin-top: -20px;" src="<?= UPLOADS?>logo.png"></span></a></h1>
                <nav role="navigation" >
                    <ul>
                        <li><a href="<?= site_url() ?>"><?= $this->lang->line('home'); ?></a></li>
                        <li><a href="<?= site_url().'/iptv_packages' ?>"><?= $this->lang->line('IPTV'); ?></a></li>
                        <li role="presentation" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                                <?= $this->lang->line('pricing_pilling'); ?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="<?= site_url().'/iptv_pricing' ?>"><?= $this->lang->line('iptv_pricing'); ?></a></li>
                                <li><a href="<?= site_url().'/cccam_pricing' ?>"><?= $this->lang->line('cccam_pricing'); ?></a></li>
                                <li><a href="<?= site_url().'/saving_offers' ?>"><?= $this->lang->line('saving'); ?></a></li>
                                <li role="separator" class="divider"></li>
                                <li><a href="<?= site_url().'/payment' ?>"><?= $this->lang->line('payment'); ?></a></li>
                            </ul>
                        </li>
                        <li><a href="<?= site_url().'/cccam_account' ?>"><?= $this->lang->line('CCcam'); ?></a></li>
                        <li role="presentation" class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-haspopup="true" aria-expanded="false">
                                <?= $this->lang->line('support'); ?> <span class="caret"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="http://maksharing.com/clientarea?language=<?=$this->session->userdata['site_lang']?>"><?= $this->lang->line('support'); ?></a></li>
                                <li><a href="<?= site_url().'/videos' ?>"><?= $this->lang->line('videos'); ?></a></li>

                            </ul>
                        </li>
                        <li><a href="<?= site_url().'/faq' ?>"><?= $this->lang->line('faq'); ?></a></li>
                        
                        <select onchange="javascript:window.location.href='<?php echo base_url(); ?>LanguageSwitcher/switchLang/'+this.value;">
                            <option value="english" <?php if($this->session->userdata('site_lang') == 'english') echo 'selected="selected"'; ?>>English</option>
                            <option value="arabic" <?php if($this->session->userdata('site_lang') == 'arabic') echo 'selected="selected"'; ?>>العربية</option>
                        </select>
                    </ul>
                </nav>

            </div>
        </div>
    </header>
