<?php $this->load->view("header"); ?>
<aside id="fh5co-hero" clsas="js-fullheight">
	<div class="flexslider js-fullheight">
		<ul class="slides">

			<li style="background-image: url(<?= UPLOADS ?>slide_3.jpg);">
				<div class="overlay-gradient"></div>
				<div class="container">
					<div class="col-md-10 col-md-offset-1 text-center js-fullheight slider-text">
						<div class="slider-text-inner">
							<h2><?= $this->lang->line('cccam_pricing'); ?></h2>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
</aside>

<div class="fh5co-pricing">
	<div class="container">


		<div class="row">
			<div class="pricing">
				<?php if ($packages): ?>
					<?php foreach ($packages as $package): ?>
						<?php if ($package['type'] == 1): ?>
							<div class="col-md-3 animate-box"  <?php if ($this->session->userdata('lang') == 'ar') echo "style='float:right'"; ?>>
								<div class="price-box">
									<h2><?= $package['name_'.$this->session->userdata('lang')] ?></h2>
									<div class="price"><sup class="currency"><?= @$package['currency']?></sup><?= $package['price']?><small>/<?= $package['period_'.$this->session->userdata('lang')]?></small></div>
									<p><?= str_replace(PHP_EOL, '<br>', $package['content_'.$this->session->userdata('lang')])?> </p>
									<a href="<?= $package['url']?>" class="btn btn-select-plan btn-sm"> <?= $this->lang->line('order'); ?></a>
								</div>
							</div>
						<?php endif;?>
					<?php endforeach; ?>
				<?php endif; ?>
			</div>
		</div>


	</div>
</div>

</div>


<?php $this->load->view("footer"); ?>
