<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Videos_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_videos()
	{
		$sql = "SELECT * FROM `videos` ORDER BY id DESC ";
		$query = $this->db->query($sql, array());

		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}


}


/* End of file pages_model.php */
/* Location: ./application/models/pages_model.php */
