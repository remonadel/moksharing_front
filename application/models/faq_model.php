<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Faq_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_questions_ar()
	{
		$sql = "SELECT * FROM `faq` WHERE question_ar !='' AND answer_ar !='' ORDER BY id DESC ";
		$query = $this->db->query($sql, array());

		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}

    public function get_questions_en()
    {
        $sql = "SELECT * FROM `faq` WHERE question_en !='' AND answer_en !='' ORDER BY id DESC ";
        $query = $this->db->query($sql, array());

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

}


/* End of file pages_model.php */
/* Location: ./application/models/pages_model.php */
