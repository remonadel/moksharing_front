<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iptv_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_iptv_channels($category_id)
	{
		$sql = "SELECT * FROM `subcategories` WHERE category_id = ? ORDER BY id DESC ";
		$query = $this->db->query($sql, array($category_id));

		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}

    public function get_iptv_categories()
    {
        $sql = "SELECT * FROM `categories` ORDER BY id DESC ";
        $query = $this->db->query($sql, array());

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

}


/* End of file pages_model.php */
/* Location: ./application/models/pages_model.php */
