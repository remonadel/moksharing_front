<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pricing_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_packages()
	{
		$sql = "SELECT * FROM `pricing` ";
		$query = $this->db->query($sql, array());

		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}

}


/* End of file pages_model.php */
/* Location: ./application/models/pages_model.php */
