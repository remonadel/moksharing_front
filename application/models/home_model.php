<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }


    public function get_main_slider_banners()
	{
		$sql = "SELECT * FROM `banners` WHERE id <5 ORDER BY id ASC ";
		$query = $this->db->query($sql, array());

		return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
	}

    public function get_secondary_slider_banners()
    {
        $sql = "SELECT * FROM `banners` WHERE id >= 5 AND id < 12 ORDER BY id ASC";
        $query = $this->db->query($sql, array());

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }

    public function get_saving_slider_banners()
    {
        $sql = "SELECT * FROM `banners` WHERE id >=12 ORDER BY id ASC";
        $query = $this->db->query($sql, array());

        return ($query->num_rows() >= 1) ? $query->result_array() : FALSE;
    }


}


/* End of file home_model.php */
/* Location: ./application/modules/banners/models/banners_model.php */
