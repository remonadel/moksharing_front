<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Iptv_packages extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model("iptv_model");
		$this->load->model("home_model");
	}
	public function index()
	{

		$data["slide"] = $this->home_model->get_main_slider_banners();
		$data['title'] = $this->lang->line('maksharing_title'). ' | '. $this->lang->line('IPTV');
		$categories = $this->iptv_model->get_iptv_categories();
		foreach ($categories as &$category)
		{
			$category['channels'] = $this->iptv_model->get_iptv_channels($category['id']);
		}
		$data['categories'] = $categories;

		$this->load->view('iptv_view', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */