<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome_page extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model("pages_model");
		$this->load->model("home_model");
	}

	public function index($id)
	{
		$data["page"] = $this->pages_model->get_page($id);
		if (!$id OR !$data['page']) redirect(site_url());
		$data["slide"] = $this->home_model->get_main_slider_banners();
		$data['title'] = $this->lang->line('maksharing_title'). ' | '. $this->lang->line($data['page']['name']);
		$this->load->view('page_view', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */