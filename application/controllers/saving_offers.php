<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Saving_offers extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function __construct()
	{
		parent::__construct();
		$this->load->model("home_model");
	}

	public function index()
	{
		$data['title'] = $this->lang->line('maksharing_title'). ' | '. $this->lang->line('saving');
		$data["slide"] = $this->home_model->get_main_slider_banners();
		$data["banners"] = $this->home_model->get_saving_slider_banners();
		$this->load->view('saving_offers_view', $data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */